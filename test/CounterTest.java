import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;

/**
 * User: Jason Weng
 * Date: 13年12月8日
 * Time: 下午5:30
 */
public class CounterTest {
    @Test
    public void testBuildCounter() throws Exception {

        Counter<String> counter = new Counter<String>(200) ;
        counter.incCount("1");

    }

    @Test
    public void testGetKeySet() throws Exception {
        Counter<String> counter = new Counter<String>(200) ;
        counter.incCount("1");
        Set s = new HashSet();
        s.add("1");
        assertEquals(s,counter.getKeySet());
        s.add("2");
        assertFalse(s.equals(counter.getKeySet()));
    }

    @Test
    public void testGetCount() throws Exception {

    }

    @Test
    public void testIncCount() throws Exception {

    }

    @Test
    public void testDecCount() throws Exception {

    }
}

import org.junit.Test;

import static junit.framework.Assert.assertEquals;

/**
 * User: Jason Weng
 * Date: 13年12月8日
 * Time: 下午7:25
 */
public class HdpUtilTest {
    @Test
    public void testFormBigram() throws Exception {
        String head = "1";
        String tail = "2";
        String bigram = HdpUtil.formBigram(head,tail);
        assertEquals(bigram,"1 2");
    }

    @Test
    public void testTailOfBigram() throws Exception {
       String bigram = "xx yy";
       String head = HdpUtil.tailOfBigram(bigram);
       assertEquals(head, "yy");
    }

   @Test(expected=AssertionError.class)
    public void testTailOfBigramThrowsAssertError() throws Exception {
       String bigram = "xx  yy";
       String head = HdpUtil.tailOfBigram(bigram);
       assertEquals(head, "yy");
    }
}

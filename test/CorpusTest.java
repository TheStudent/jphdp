import org.junit.Test;

import static junit.framework.Assert.assertEquals;

/**
 * User: Jason Weng
 * Date: 13年12月1日
 * Time: 下午4:34
 */
public class CorpusTest {
    @Test
    public void testLoadCorpus() throws Exception {
        String path =  System.class.getResource("/1.txt").getPath() ;
        Corpus corpus = Corpus.LoadCorpus(path) ;
        assertEquals("abc",corpus.getContent());
        assertEquals(3, corpus.getCorpSize());
        assertEquals(3,corpus.getCorpCharSetSize());
    }

    @Test
    public void testParseContext() throws Exception {
        String path =  System.class.getResource("/1.txt").getPath() ;
        Corpus corpus = Corpus.LoadCorpus(path) ;
        corpus.parseContext(0);
    }

    @Test
    public void testWriteToFile() throws  Exception {
        String in_path =  System.class.getResource("/1.txt").getPath() ;
        String path = getClass().getResource("/1o.txt").getPath();
        Corpus corpus = Corpus.LoadCorpus(in_path);
        corpus.writeToFile(path);
    }
}

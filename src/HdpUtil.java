import com.google.common.base.CharMatcher;

import java.util.List;

/**
 * User: Jason Weng
 * Date: 13年11月24日
 * Time: 下午10:22
 */
public class HdpUtil {

   public static boolean bernuolio (double p){
       //May not be precise enough. come back later.
       double r = Math.random();
       return r <= p ? true:false;
   }

    public static String formBigram(String head, String tail){
        assert(head.length() >0 && tail.length() >0);
        assert(!head.contains(" ") && !tail.contains(" "));
        return head+" "+tail;
    }

    public static String tailOfBigram(String bigram){
        assert(CharMatcher.is(' ').countIn(bigram) == 1);
        int index = bigram.indexOf(' ');
        return bigram.substring(index+1);
    }

    public static void incCountFromListAsBigram(Counter<String> counter, List<String> keylist){
        int size = keylist.size();
        for(int i=0;i<size-1;i++){
            String pre = keylist.get(i);
            String tail = keylist.get(i+1);
            String bigram = formBigram(pre, tail);
            counter.incCount(bigram);
        }
    }



}

import java.io.IOException;
import java.util.List;

/**
 * User: Jason Weng
 * Date: 13年12月8日
 * Time: 下午6:57
 */

enum Action {
    ADD, REMOVE
}

public class HDPTrain {
     private final Corpus corpus;

     private double pchar;

     private int totalTableNumber;

     private Counter<String> bigramCounter = new Counter<String>(500);

     private Counter<String> wordCounter = new Counter<String>(300);

     private Counter<String> tableCounter = new Counter<String>(500);

     private Histogram histogram = Histogram.buildHistogram();


     public HDPTrain(String inPath, String outPath) throws IOException {
        this.corpus = Corpus.LoadCorpus(inPath);
        List<String> wordList = corpus.getWordList();
        wordCounter.incCountFromList(corpus.getWordList());
      //  HdpUtil.incCountFromListAsBigram(bigramCounter, corpus.getWordList());
        int wordListSize =   wordList.size();
         for(int i=0;i<wordListSize-1;i++){
             String pre = wordList.get(i);
             String tail = wordList.get(i+1);
             String bigram = HdpUtil.formBigram(pre, tail);
             this.inTable(bigram);
         }
     }



     private double base_p1(String word){
         int tableNumber = this.tableCounter.getCount(word);
         double p0 = Formula.baseWordProb(this.pchar,word);
         int wordNumber = this.wordCounter.getCount(word);
         return (tableNumber + Formula.ALPAH0 * p0 ) /(wordNumber + Formula.ALPAH0);
     }

     private void inTable(String bigram){
        int customers = this.bigramCounter.getCount(bigram);
        String word = HdpUtil.tailOfBigram(bigram);
        double p1 = Formula.ALPHA1 * base_p1(word);
        double pnew =  p1 /(p1 + customers);
        boolean isNewTable = Histogram.incHisto(bigram, customers,pnew,histogram);
        bigramCounter.incCount(bigram);
        if(isNewTable){
            tableCounter.incCount(word);
            totalTableNumber += 1;
        }
     }


    private void leaveTable(String bigram){
        int customers = this.bigramCounter.getCount(bigram);
        String word =  HdpUtil.tailOfBigram(bigram);
        boolean isTableGone = Histogram.decHisto(bigram,customers, histogram);
        if(isTableGone){
            tableCounter.decCount(word);
            totalTableNumber -= 1;
        }
    }

    private void clearContext(int pos){

    }


    private void updateContext(int pos, Action act){
        switch (act) {
            case ADD:
            case REMOVE:
        }
    }


    private void do_gibbs_sampling(int pos){

    }
}

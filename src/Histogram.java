import java.util.Hashtable;

/**
 * User: Jason Weng
 * Date: 13年12月8日
 * Time: 下午4:28
 */
public class Histogram {

       private Hashtable<String, Counter<Integer>> histo_dict;

       private Histogram(int initSize) {
             this.histo_dict = new Hashtable<String, Counter<Integer>>(initSize);
       }

       public static Histogram buildHistogram(){
           return new Histogram(300);
       }

       public Histogram  buildHistogram(int size){
          return new Histogram(size);
       }

       public Counter getHisto(String key){
           if(this.histo_dict.contains(key)) {
                 return this.histo_dict.get(key);
           }   else {
               Counter counter = new Counter<Integer>(200);
               this.histo_dict.put(key, counter);
               return this.histo_dict.get(key);
           }
       }

       public static int stopAt(final Counter<Integer> counter, int totalCustomer){
           assert(counter.getKeySet().size() > 0);
           assert(totalCustomer >0);
           int pos = (int) (Math.random() * totalCustomer);      //need a better way to calculate the random number
           for(Integer key: counter.getKeySet())   {
                   int v = counter.getCount(key);
                   pos -= key*v;
                   if(pos <= 0) {
                       pos = key;
                       break;
                   }
           }
           return pos;
       }


       public static boolean incHisto(String bigram,int coustomerNumber, double p, Histogram histo) {
           Counter<Integer> counter = histo.getHisto(bigram);
           boolean isNewtable = HdpUtil.bernuolio(p);
           if(isNewtable){
              counter.incCount(1);
              return true;
           }  else {
              int k = stopAt(counter,coustomerNumber)  ;
               counter.decCount(k);
               counter.incCount(k+1);
               return false;
           }
       }

       public static boolean decHisto(String bigram,int coustomerNumber, Histogram histo) {
           Counter<Integer> counter = histo.getHisto(bigram);
           int k = stopAt(counter,coustomerNumber)  ;
           counter.decCount(k);
           if(k > 1)  {
               counter.incCount(k-1);
               return false;
           }else {
               return true;  // a existing table is gone
           }
       }

}

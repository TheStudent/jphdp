/**
 * User: Jason Weng
 * Date: 13年11月24日
 * Time: 下午10:51
 */
public class Formula {

    public static final double CONT = 0.5;

    public static final int ALPAH0 = 2000;

    public static final int ALPHA1 = 100;

    public static double clac_probOfChar(int charsetSize){
        return  1.0/charsetSize;
    }

    public static double baseWordProb(double charProb, String word){
        int wl = word.length();
        return Math.pow(CONT, wl-1) * Math.pow(charProb, wl) * (1 -CONT);
    }

}

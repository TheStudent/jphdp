import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;

/**
 * User: Jason Weng
 * Date: 13年12月1日
 * Time: 下午2:14
 */
public class Counter<KeyType> {
    //needs to make it genereic later

    private Hashtable<KeyType, Integer> dict;

    public Counter(int initialSize) {
        this.dict = new Hashtable<KeyType, Integer>(initialSize);
    }

    public Set<KeyType> getKeySet() {
        return this.dict.keySet();
    }
    //anyway to support this?
   /* public static <T> Counter buildCounter(){
        Counter<T> counter = new Counter<T> (200);
        return counter;
    }

    public static<T> Counter buildCounter(int initSize){
        Counter<T> counter = new Counter<T> (initSize);
        return counter;
    }*/


    public int getCount(KeyType key) {
        Integer n = this.dict.get(key);
        if (n == null) return 0;
        else return n;
    }


    public void incCount(KeyType key) {
        if (this.dict.contains(key)) {
            int n = this.dict.get(key);
            this.dict.put(key, n + 1);
        } else {
            this.dict.put(key, 1);
        }
    }


    public void incCountFromList(List<KeyType> keylist) {
        for (KeyType k : keylist) {
            this.incCount(k);
        }
    }


    public void decCount(KeyType key) {
        assert (this.dict.contains(key));
        int n = this.dict.get(key);
        if (n == 1) this.dict.remove(key);
        else this.dict.put(key, n - 1);

    }
}

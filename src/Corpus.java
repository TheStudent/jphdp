import java.io.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * User: Jason Weng
 * Date: 13年12月1日
 * Time: 下午2:27
 */
public class Corpus {

    private final String content;

    private final int corpSize;

    private final Set<Character> corpCharSet;
    //need to look into primitive collection later.

    private final int corpCharSetSize;

    private boolean[] status;     //true for link. false for break.  Using enums with meaningful names may be better.

    public String getContent() {
        return content;
    }

    public int getCorpSize() {
        return corpSize;
    }

    public Set<Character> getCorpCharSet() {
        return corpCharSet;
    }

    public int getCorpCharSetSize() {
        return corpCharSetSize;
    }

    public boolean[] getStatus() {
        return status;
    }

    private void initializeStatus(int size) {
        this.status = new boolean[corpSize];
        for (int i = 0; i < corpSize - 1; i++) {
            status[i] = HdpUtil.bernuolio(0.5);
        }
        status[corpSize - 1] = false;
    }

    private Set<Character> parseCorpSet(String text) {
        Set<Character> cs = new HashSet<Character>();
        for (int i = 0; i < text.length(); i++) {
            char c = text.charAt(i);
            cs.add(c);
        }
        return cs;
    }


    public Corpus(String content) {
        this.content = content;
        this.corpSize = content.length();
        this.corpCharSet = parseCorpSet(content);
        this.corpCharSetSize = this.corpCharSet.size();
        this.initializeStatus(this.corpSize);
    }

    private static String readFileToString(String path) throws IOException {
        StringBuffer stringBuffer = new StringBuffer();
        try (BufferedReader br =
                     new BufferedReader(new FileReader(path))) {
            String line;
            while ((line = br.readLine()) != null) {

                stringBuffer.append(line);
            }

        }
        return stringBuffer.toString();
    }

    public static Corpus LoadCorpus(String path) throws IOException {
        String content = readFileToString(path);
        Corpus corpus = new Corpus(content);
        return corpus;

    }

    private int parseLeftBound(int pos){
        int lbPos = pos - 1;
        if(lbPos == -1  || !this.status[lbPos])     return pos;
        else return parseLeftBound(lbPos);
    }

    private int parseRightBound(int pos){
        int rbPos = pos + 1;
        if(! this.status[rbPos]) return rbPos;
        else return parseRightBound(rbPos);
    }


    private String parseLeftWord(int pos){
        if(pos == 0) return null;
        else {
            int lb = parseLeftBound(pos -1);
            return this.content.substring(lb, pos);
        }
    }


    private String parseRightWord(int pos){
        if ( pos == this.corpSize - 1) return null;
        else {
            int rb = parseRightBound( pos);
            return  this.content.substring(pos+1, rb+1);
        }
    }
    public Context parseContext(int pos){
          int lb = parseLeftBound(pos);
          int rb = parseRightBound(pos);
          String w1 = this.content.substring(lb,pos+1);
          String w2 = this.content.substring(pos+1, rb+1);
          String w3 = w1+w2;
          String lw = parseLeftWord(lb);
          String rw = parseRightWord(rb);
          return new Context(lw,w1,w2,w3, rw);
    }

    public List<String> getWordList() {
        List<String> words = new ArrayList<String>();

        StringBuffer word = new StringBuffer();
        for(int i=0;i<this.corpSize;i++){
            word.append(this.content.charAt(i));
            if(!status[i]) {
                word.append(' ');
                words.add(word.toString());
                word.delete(0, word.length());
            }
        }
        return words;
    }


    public void writeToFile(String path) throws  IOException{
        List<String> words = this.getWordList();
        try (BufferedWriter bw =
                     new BufferedWriter(new FileWriter(path))) {
          for(String w: words) bw.write(w);
          bw.flush();
        }

    }


}

/**
 * User: Jason Weng
 * Date: 13年12月1日
 * Time: 下午2:21
 */
public class Context {
    private final String leftw;
    private final String w1;
    private final String w2;
    private final String w3;
    private final String rightw;

    public Context(String leftw, String w1, String w2, String w3, String rightw) {
        this.leftw = leftw;
        this.w1 = w1;
        this.w2 = w2;
        this.w3 = w3;
        this.rightw = rightw;
    }
}
